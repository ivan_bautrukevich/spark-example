package com.exadel.task5;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

public class Convertor {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: WordCount <file>");
            System.exit(0);
        }

        convertor(args[0]);
    }


    public static void convertor(String filename) {
        // Define a configuration to use to interact with Spark
        SparkConf conf = new SparkConf().setMaster("local").setAppName("Work Count App");

        // Create a Java version of the Spark Context from the configuration
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession
                .builder()
                .appName("Convert CSV to Parquet")
                .config("spark.some.config.option", "some-value")
                .getOrCreate();

        SQLContext sqlContext = new SQLContext(spark);

        Dataset<Row> dataset =  sqlContext.read().format("csv").option("header", "true").csv(filename);
        dataset.write().parquet("test-parquet");
        dataset.printSchema();
    }
}
