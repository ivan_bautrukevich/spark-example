package com.exadel.task5.csvgenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.util.Random;


public class CSV {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("test.csv");
        PrintWriter pw = new PrintWriter(file);
        while (file.length() <= 100_048_576) {
            writeRow(pw);
        }
        pw.close();
        System.out.println("done!");
    }

    private static void writeRow(PrintWriter pw) {
        StringBuilder sb = new StringBuilder();

        sb.append(timeStamp());
        sb.append(',');
        sb.append(phoneNumber());
        sb.append(',');
        sb.append(phoneNumber());
        sb.append(',');
        sb.append(new Random().nextInt(10000) + 1);
        sb.append(',');
        sb.append(randomPlace());
        sb.append(',');
        sb.append(randomPlace());
        sb.append('\n');

        pw.write(sb.toString());
    }

    private static String timeStamp(){
        Random generator = new Random();
        return LocalTime.MIN.plusSeconds(generator.nextLong()).toString();
    }

    private static String phoneNumber() {
        Random rand = new Random();
        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(10000);

        DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
        DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

        return df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);
    }

    private static String randomPlace() {
        String[] array = {"minsk", "brest", "vitebsk"};
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }
}
