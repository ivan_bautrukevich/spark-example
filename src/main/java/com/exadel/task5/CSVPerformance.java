package com.exadel.task5;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.*;

public class CSVPerformance {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: WordCount <file>");
            System.exit(0);
        }
        convertor(args[0]);
    }


    public static void convertor(String filename) {
        // Define a configuration to use to interact with Spark
        SparkConf conf = new SparkConf().setMaster("local").setAppName("Work Count App");

        // Create a Java version of the Spark Context from the configuration
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession
                .builder()
                .appName("Compare performance Parquet vs CSV")
                .config("spark.some.config.option", "some-value")
                .getOrCreate();

        SQLContext sqlContext = new SQLContext(spark);

        Dataset<Row> dataset =  sqlContext.read().format("csv").option("header", "true").csv(filename);
        dataset.printSchema();
        dataset.show();

        long start;
        long end;
        System.out.println(start = System.currentTimeMillis());
        dataset.groupBy("from").agg(sum("duration").as("sum_duration")).sort(col("sum_duration").desc()).limit(10).show();
        System.out.println(end = System.currentTimeMillis());
        System.out.println("time " + (end - start));
//        time 11128
//        time 13358
    }
}
