package com.exadel.sparkexample;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import static org.apache.spark.sql.functions.col;

public class DataSetExample {
    public static void wordCountJava8(String filename) {
        // Define a configuration to use to interact with Spark
        SparkConf conf = new SparkConf().setMaster("local").setAppName("Work Count App");

        // Create a Java version of the Spark Context from the configuration
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL basic example")
                .config("spark.some.config.option", "some-value")
                .getOrCreate();


        Dataset<University> schools = spark.read()
                .json(filename)
                .as(Encoders.bean(University.class));

        schools.map(new BuildString(), Encoders.STRING());
        schools.show();
        schools.filter(col("yearFounded").gt(1860)).show();
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: WordCount <file>");
            System.exit(0);
        }

        wordCountJava8(args[0]);
    }
}
