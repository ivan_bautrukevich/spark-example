package com.exadel.sparkexample;

import java.io.Serializable;

public class University implements Serializable {
    private String name;
    private long numStudents;
    private long yearFounded;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNumStudents() {
        return numStudents;
    }

    public void setNumStudents(long numStudents) {
        this.numStudents = numStudents;
    }

    public long getYearFounded() {
        return yearFounded;
    }

    public void setYearFounded(long yearFounded) {
        this.yearFounded = yearFounded;
    }
}