package com.exadel.sparkexample;

import org.apache.spark.api.java.function.MapFunction;

class BuildString implements MapFunction<University, String> {

    @Override
    public String call(University u) throws Exception {
        return u.getName()
                + " is "
                + (2015 - u.getYearFounded())
                + " years old.";
    }
}

