package com.exadel.task6;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.stat.Statistics;
import org.apache.spark.mllib.stat.test.ChiSqTestResult;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class StudentHypothesis {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: StudentHypothesis <file>");
            System.exit(0);
        }
        hypothesis(args[0]);
    }

    private static void hypothesis(String filename) {
        // Define a configuration to use to interact with Spark
        SparkConf conf = new SparkConf().setMaster("local").setAppName("Work Count App");

        // Create a Java version of the Spark Context from the configuration
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession
                .builder()
                .appName("Hypothesis")
                .getOrCreate();
        SQLContext sqlContext = new SQLContext(spark);
        StructType structType = new StructType(new StructField[] {
                new StructField("id", DataTypes.LongType, false, Metadata.empty()),
                new StructField("firstName", DataTypes.StringType, false, Metadata.empty()),
                new StructField("lastName", DataTypes.StringType, false, Metadata.empty()),
                new StructField("score", DataTypes.DoubleType, false, Metadata.empty()),
        });

        Dataset<Row> dataset = sqlContext.read().format("csv").option("header", "true").schema(structType).csv(filename);
        dataset.printSchema();
        dataset.show();

        Vector observedVector = HypothesisUtil.getObservedFrequenciesVector(dataset, HypothesisUtil.INTERVAL_COUNT);
        Vector expectedVector = HypothesisUtil.getExpectedFrequenciesVectorForStudent(HypothesisUtil.INTERVAL_COUNT);
        ChiSqTestResult result = Statistics.chiSqTest(observedVector, expectedVector);

        System.out.println("Degrees of freedom " + result.degreesOfFreedom());
        System.out.println("Statistic " + result.statistic());
        System.out.println("p-value: " + result.pValue());
        System.out.println("Result: " + result.nullHypothesis());
    }
}