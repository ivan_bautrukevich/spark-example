package com.exadel.task6.generator;

import com.exadel.task6.HypothesisUtil;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import static org.apache.spark.mllib.random.RandomRDDs.normalJavaRDD;

public class StudentGenerator {

    public static void main(String[] args) throws FileNotFoundException {
        // Define a configuration to use to interact with Spark
        SparkConf conf = new SparkConf().setMaster("local").setAppName("Work Count App");

        // Create a Java version of the Spark Context from the configuration
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession
                .builder()
                .appName("Student generator")
                .getOrCreate();
        JavaDoubleRDD standardNormal = normalJavaRDD(JavaSparkContext.fromSparkContext(spark.sparkContext()), HypothesisUtil.STUDENT_SIZE);
        List<Double> scores = standardNormal.mapToDouble(x -> HypothesisUtil.MEDIAN + Math.sqrt(HypothesisUtil.DISPERSION) * x).take(HypothesisUtil.STUDENT_SIZE);
        Long result = writeNormalRdd(scores);
        System.out.println(result);
    }

    private static Long writeNormalRdd(List<Double> scores) throws FileNotFoundException {
        File file = new File("student.csv");
        PrintWriter pw = new PrintWriter(file);
        initHeader(pw);
        Long id = 0L;
        for (Double value : scores) {
            if (value >= HypothesisUtil.MIN && value <= HypothesisUtil.MAX) {
                writeRow(id, pw, value);
                id++;
            }
        }
        pw.close();
        return id;
    }

    private static void initHeader(PrintWriter pw) {
        StringBuilder sb = new StringBuilder();

        sb.append("id");
        sb.append(',');
        sb.append("firstName");
        sb.append(',');
        sb.append("lastName");
        sb.append(',');
        sb.append("score");
        sb.append('\n');

        pw.write(sb.toString());
    }

    private static void writeRow(Long i, PrintWriter pw, Double score) {
        StringBuilder sb = new StringBuilder();

        sb.append(i);
        sb.append(',');
        sb.append("name" + i);
        sb.append(',');
        sb.append("lastName" + i);
        sb.append(',');
        sb.append(score);
        sb.append('\n');

        pw.write(sb.toString());
    }
}