package com.exadel.task6;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.util.ArrayList;
import java.util.List;

public class HypothesisUtil {

    public static final int STUDENT_SIZE = 1000;
    public static final double DISPERSION = 2.25;
    public static final double MIN = 1.0;
    public static final double MAX = 10.0;
    public static final double MEDIAN = 5.5;
    public static final double INTERVAL = 0.1;
    public static final int INTERVAL_COUNT = (int) ((MAX - MIN) / INTERVAL);

    public static final String FILTER_VALUE_BETWEEN_QUERY = "value > %f and value <= %f";

    public static Vector getObservedFrequenciesVector(Dataset<Row> dataset, int intervals) {
        Dataset<Double> scores = dataset.map((MapFunction<Row, Double>) row -> row.getAs("score"), Encoders.DOUBLE());
        double[] scoreData = new double[intervals];
        double start = MIN;
        double end = MIN + INTERVAL;
        //count values in interval
        for (int i = 0; i < intervals; i++) {
            String filterQuery = String.format(FILTER_VALUE_BETWEEN_QUERY, start, end);
            long count = scores.filter(filterQuery).count();
            scoreData[i] = count;
            start = end;
            end = start + INTERVAL;
        }
        return Vectors.dense(scoreData);
    }

    public static Vector getExpectedFrequenciesVectorForStudent(int count) {
        List<Double> expectedFrequencies = expectedFrequencies(count);
        double[] expected = new double[count];
        for (int i = 0; i < count; i++) {
            expected[i] = expectedFrequencies.get(i);
        }
        return Vectors.dense(expected);
    }

    private static List<Double> expectedFrequencies(int count) {
        List<Double> result = new ArrayList<>(count);
        double middleInterval = MIN + INTERVAL / 2;
        for (int i = 0; i < count; i++) {
            double ti = (middleInterval - MEDIAN) / Math.sqrt(DISPERSION);
            double theoreticalValue = INTERVAL * count * fi(ti) / MEDIAN;
            result.add(theoreticalValue);

            middleInterval += INTERVAL;
        }
        return result;
    }

    private static double fi(double t) {
        return Math.exp(-(t * t) / 2) / (Math.sqrt(2 * Math.PI));
    }
}