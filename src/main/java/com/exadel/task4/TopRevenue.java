package com.exadel.task4;

import com.exadel.sparkexample.University;
import com.exadel.task4.entity.Order;
import com.exadel.task4.entity.OrderItem;
import com.exadel.task4.entity.Product;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;

import static jdk.nashorn.internal.objects.NativeMath.max;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.collect_set;
import static org.apache.spark.sql.functions.sum;

public class TopRevenue {
    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Usage: TopRevenue <files>");
            System.exit(0);
        }
        convertor(args);
    }


    public static void convertor(String[] args) {
        // Define a configuration to use to interact with Spark
        SparkConf conf = new SparkConf().setMaster("local")
                .setAppName("Top Revenue App");

        // Create a Java version of the Spark Context from the configuration
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession spark = SparkSession
                .builder()
                .appName("Product Category find top 3 products which produce top revenue")
                .config("spark.some.config.option", "some-value")
                .getOrCreate();

        Dataset<Order> orders = spark.read().json(args[0]).as(Encoders.bean(Order.class));

        Dataset<OrderItem> orderItems = spark.read().json(args[1]).as(Encoders.bean(OrderItem.class));

        Dataset<Row> orderItemsMult = orderItems.withColumn("multiply", orderItems.col("qty").multiply(orderItems.col("cost")));

        Dataset<Product> products = spark.read().json(args[2]).as(Encoders.bean(Product.class));

        Dataset<Row> productsWithItem = orderItemsMult.join(orders, orderItemsMult.col("order_id").equalTo(orders.col("order_id")));
        productsWithItem.printSchema();
        productsWithItem.show();
        Dataset<Row> dataset = products.join(productsWithItem, products.col("product_id").equalTo(productsWithItem.col("product_id")));
        dataset.printSchema();
        dataset.show();
        WindowSpec windowSpec = Window.partitionBy(col("category_id")).orderBy(col("multiply").desc());
        Column rowNum = functions.row_number().over(windowSpec);
        Dataset<Row> result = dataset.select(dataset.col("category_id"), orderItemsMult.col("product_id"), orderItemsMult.col("multiply"),rowNum.as("row_number")).filter(col("row_number").$less$eq(5));
        result.printSchema();
        result.show();
    }
}
