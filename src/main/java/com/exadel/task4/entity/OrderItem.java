package com.exadel.task4.entity;

import java.util.Objects;

public class OrderItem {
    private long item_id;
    private long order_id;
    private long product_id;
    private int qty;
    private long promotion_id;
    private float cost;

    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public long getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(long promotion_id) {
        this.promotion_id = promotion_id;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return item_id == orderItem.item_id &&
                order_id == orderItem.order_id &&
                product_id == orderItem.product_id &&
                qty == orderItem.qty &&
                promotion_id == orderItem.promotion_id &&
                Float.compare(orderItem.cost, cost) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(item_id, order_id, product_id, qty, promotion_id, cost);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "item_id=" + item_id +
                ", order_id=" + order_id +
                ", product_id=" + product_id +
                ", qty=" + qty +
                ", promotion_id=" + promotion_id +
                ", cost=" + cost +
                '}';
    }
}
