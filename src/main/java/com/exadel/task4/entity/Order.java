package com.exadel.task4.entity;

import java.sql.Timestamp;
import java.util.Objects;

public class Order {
    private long order_id;
    private long customer_id;
    private Timestamp creation_tmst;
    private Timestamp completion_tmst;

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(long customer_id) {
        this.customer_id = customer_id;
    }

    public Timestamp getCreation_tmst() {
        return creation_tmst;
    }

    public void setCreation_tmst(Timestamp creation_tmst) {
        this.creation_tmst = creation_tmst;
    }

    public Timestamp getCompletion_tmst() {
        return completion_tmst;
    }

    public void setCompletion_tmst(Timestamp completion_tmst) {
        this.completion_tmst = completion_tmst;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return order_id == order.order_id &&
                customer_id == order.customer_id &&
                Objects.equals(creation_tmst, order.creation_tmst) &&
                Objects.equals(completion_tmst, order.completion_tmst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(order_id, customer_id, creation_tmst, completion_tmst);
    }

    @Override
    public String toString() {
        return "Order{" +
                "order_id=" + order_id +
                ", customer_id=" + customer_id +
                ", creation_tmst=" + creation_tmst +
                ", completion_tmst=" + completion_tmst +
                '}';
    }
}
